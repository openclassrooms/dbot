# dbot

Just another Discord bot.  

## Configuration

Use environment variables:  

|  name  |  description  |  default value  |
|---|---|---|
|  `DISCORD_TOKEN` |  Discord application token  |    |
|  `OWNER_ID` |  Bot owner ID  |    |
|  `BOT_LOG_LEVEL` |  log level (possible values: `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`)  |  `INFO`  |
|  `BOT_COMMAND_PREFIX` |  command prefix (one character) |  `!`  |

## Road map

  - ~~**version 0.1:** core events *(bot connection, member join/left guild, i18n)*~~
  - ~~**version 0.2:** core command *(guild stats, bot latency and kick member)*~~
  - ~~**version 0.3:** discord help *(Discord commands cheat sheet)*~~
  - ~~**version 0.4:** poll command *(manage poll)*~~
  - ~~**version 0.5:** multi-guid system and welcome events *(using a database)*~~
  - **version 0.6:** admin *(manage guild stats/logs, clear channels and ban/unban a member)* and 
  bot *(enable/disable bot/plugin per-server or global, display bot id card)* commands
  - **version 0.7:** resources command *(manage resources)*
  - **version 0.8:** good day wishes task *(wish a good day using an API)*
  - **version 0.9:** admin task *(display stats every week)*

## Sources

[discord.py - source code](https://github.com/Rapptz/discord.py)  
[discord.py - documentation](https://discordpy.readthedocs.io/en/latest/)  
