import gettext

import discord

from datetime import datetime as dt
from sys import stdout, stderr


# Owner -->
WEBSITE_URL = 'https://bitbucket.org/openclassrooms/dbot/src/master/'
MAINTAINER_URL = 'https://bitbucket.org/openclassrooms/dbot/issues?status=new&status=open'
# Owner <--


# Colors -->
COLOR = discord.Color.blue()
# Colors <--


# Delays -->
SHORT_DELAY = 4
MEDIUM_DELAY = 8
LONG_DELAY = 16
VERY_LONG_DELAY = 32
# Delays <--


# Database -->
DB_NAME = 'dbot.db'
# Database <--


# Log system -->
DEBUG = 5
INFO = 4
WARNING = 3
ERROR = 2
CRITICAL = 1

LEVELS = {'DEBUG': DEBUG,
          'INFO': INFO,
          'WARNING': WARNING,
          'ERROR': ERROR,
          'CRITICAL': CRITICAL}


class Console:
    """Display messages on stdout or stderr.
    Display messages on stdout with debug, info and warning methods.
    Display messages on stderr with error and critical methods.
    :param _log_level: log level (default: INFO)
    :type _log_level: str, int or constants (DEBUG, INFO, WARNING, ERROR,
    CRITICAL)
    """
    _log_level = INFO

    @staticmethod
    def set_log_level(level):
        if isinstance(level, str) and level.upper() in LEVELS.keys():
            Console._log_level = LEVELS[level.upper()]
        elif isinstance(level, int) and level in LEVELS.values():
            Console._log_level = level
        else:
            Console.warning('invalid log level')

    @staticmethod
    def get_log_level():
        return Console._log_level

    @staticmethod
    def _log(level, msg, stream=stdout):
        if LEVELS[level] <= Console._log_level:
            print(dt.now().strftime('%Y-%m-%d %H:%M:%S - ') +
                  level + ' - ' + msg, file=stream)

    @staticmethod
    def critical(msg):
        Console._log('CRITICAL', msg, stderr)

    @staticmethod
    def error(msg):
        Console._log('ERROR', msg, stderr)

    @staticmethod
    def warning(msg):
        Console._log('WARNING', msg)

    @staticmethod
    def info(msg):
        Console._log('INFO', msg)

    @staticmethod
    def debug(msg):
        Console._log('DEBUG', msg)
# Log system <--


# Markdown and more -->
class Markdown:
    """Format text with Markdown syntax."""
    @staticmethod
    def italic(string):
        return '*' + string + '*'

    @staticmethod
    def bold(string):
        return '**' + string + '**'

    @staticmethod
    def underline(string):
        return '__' + string + '__'

    @staticmethod
    def strikethrough(string):
        return '~~' + string + '~~'

    @staticmethod
    def href(string):
        return '<' + string + '>'

    @staticmethod
    def ahref(uri, msg=None):
        if msg:
            string = '[' + msg + '](' + uri + ')'
        else:
            string = '[' + uri + '](' + uri + ')'
        return string

    @staticmethod
    def code(string):
        return '`' + string + '`'

    @staticmethod
    def quote(string):
        return '> ' + string

    @staticmethod
    def spoiler(string):
        return '||' + string + '||'

    @staticmethod
    def code_block(code, style=''):
        return f'```{style}\n' + code + '\n```'

    @staticmethod
    def reverse_escape(string):
        string = string.replace('\\n', '\n').replace('\\r', '\r')  # returns
        string = string.replace('\\t', '\t').replace('\\v', '\v')  # tabs
        string = string.replace('\\"', '\"').replace("\\'", "\'")  # quotes
        return string

    @staticmethod
    def dequote(string):
        if string.startswith('"') and string.endswith('"'):
            string = string[1:-1]
        elif string.startswith("'") and string.endswith("'"):
            string = string[1:-1]
        return string
# Markdown and more <--


# Extensions -->
cogs_available = {'admin', 'discord', 'poll', 'welcome', 'goodbye', 'bot'}
cogs_enabled = set()
# Extensions <--


# Load i18n (default: english)
lang = gettext.translation('base', 'locales', fallback=True)
lang.install()
_ = lang.gettext
