from datetime import datetime, timedelta

import discord
from discord.ext import commands

from cogs import VERY_LONG_DELAY, LONG_DELAY, SHORT_DELAY, MEDIUM_DELAY, _, COLOR
from cogs import Markdown as md


class Admin(commands.Cog):

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def ping(self, ctx):
        """Get bot latency"""
        msg = _('latency: {latency}ms').format(latency=round(ctx.bot.latency * 1000))
        await ctx.send(md.code_block(msg), delete_after=LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @commands.group(aliases=['guild', 'admin'])
    @commands.has_permissions(administrator=True)
    async def server(self, ctx):
        """Admin tools"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.server)

    @server.command()
    async def status(self, ctx):
        """Get guild status"""
        guild = ctx.guild

        online_member_count = sum(
            member.status != discord.Status.offline and not member.bot for member in guild.members)
        members = '**{0}** / {1}'.format(online_member_count, guild.member_count)

        bot_count = sum(member.bot for member in guild.members)
        online_bot_count = sum(member.status != discord.Status.offline and member.bot for member in guild.members)
        bots = '**{0}** / {1}'.format(online_bot_count, bot_count)

        embed = discord.Embed(title=guild.name,
                              description=guild.description,
                              color=COLOR)
        embed.set_thumbnail(url=guild.icon_url)
        embed.add_field(name=_('Server name'), value=guild.name)
        embed.add_field(name=_('Server ID'), value=guild.id)
        embed.add_field(name=_('Region'), value=guild.region)
        embed.add_field(name=_('Categories'), value=str(len(guild.categories)))
        embed.add_field(name=_('Channels'), value=str(len(guild.channels)))
        embed.add_field(name=_('Owner'), value=guild.owner.mention)
        embed.add_field(name=_('Roles'), value=str(len(guild.roles)))
        embed.add_field(name=_('Members'), value=members)
        embed.add_field(name=_('Bots'), value=bots)
        embed.add_field(name=_('Server created at'), value=guild.created_at.date())

        await ctx.send(embed=embed, delete_after=VERY_LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @server.command(aliases=['invitation'])
    async def invitations(self, ctx):
        """Get guild invitations"""
        guild = ctx.guild
        invites = await guild.invites()

        embed = discord.Embed(title=_('Guild invitations'),
                              color=COLOR)
        embed.set_thumbnail(url=guild.icon_url)
        if invites:
            members = list()
            urls = list()
            uses = list()
            for invite in invites:
                members.append(invite.inviter.mention)
                urls.append(invite.url)
                uses.append(str(invite.uses))
            embed.add_field(name=_('Member'), value='\n'.join(members))
            embed.add_field(name=_('Url'), value='\n'.join(urls))
            embed.add_field(name=_('Uses'), value='\n'.join(uses))
        else:
            embed.add_field(name=_('Invitations'), value=_('No pending invitations.'))

        await ctx.send(embed=embed, delete_after=VERY_LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @commands.group(aliases=['member'])
    @commands.has_permissions(administrator=True)
    async def user(self, ctx):
        """Display user information"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.user)

    @user.command()
    async def info(self, ctx, member: discord.Member = None):
        """Display user information"""
        if not member:
            await ctx.send_help(self.user)
            return

        invites = await ctx.guild.invites()
        urls = list()
        uses = list()
        expiration = list()
        for invite in invites:
            if invite.inviter.id == member.id:
                urls.append(invite.url)
                uses.append(str(invite.uses))
                if invite.max_age == 0:
                    expiration.append(' - ')
                else:
                    expiration.append(
                        str(timedelta(seconds=invite.max_age) - (datetime.now() - invite.created_at)).split(".")[0])

        embed = discord.Embed(title=_('User information'),
                              color=COLOR)
        embed.set_author(name=member.display_name, icon_url=member.avatar_url)
        embed.set_thumbnail(url=member.avatar_url)

        embed.add_field(name=_('Id'), value=member.id)
        embed.add_field(name=_('Name'), value=member.display_name)
        embed.add_field(name=_('Discriminator'), value=member.discriminator)
        embed.add_field(name=_('Created at'), value=member.created_at.date())
        embed.add_field(name=_('Guild joined at'), value=member.joined_at.date())
        embed.add_field(name=_('Status'), value=str(member.status))
        if urls:
            embed.add_field(name=_('Invitation url'), value='\n'.join(urls))
            embed.add_field(name=_('Expiration'), value='\n'.join(expiration))
            embed.add_field(name=_('Uses'), value='\n'.join(uses))
        embed.add_field(name=_('Roles'), value=', '.join(a.name for a in member.roles), inline=False)
        if member.activities:
            embed.add_field(name=_('Activities'), value=', '.join(a.name for a in member.activities), inline=False)

        await ctx.send(embed=embed, delete_after=VERY_LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @user.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.Member, *, reason=None):
        """Kick a member"""
        await member.kick(reason=reason)
        await ctx.message.delete(delay=SHORT_DELAY)

    @user.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.Member, *, reason=None):
        """Ban a member"""
        await member.ban(reason=reason)
        await ctx.message.delete(delay=SHORT_DELAY)

    @user.command()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, *, member):
        """Unban a member"""
        banned_users = await ctx.guild.bans()

        name, discriminator = member.split('#')

        for ban_entry in banned_users:
            user = ban_entry.user

            if (user.name, user.discriminator) == (name, discriminator):
                await ctx.guild.unban(user)
                break

        await ctx.message.delete(delay=SHORT_DELAY)

    @commands.command(aliases=['whoami', 'myid', 'id'])
    async def me(self, ctx):
        """Display user information"""
        await ctx.invoke(self.info, ctx.author)

    @commands.group(aliases=['purge'])
    async def clean(self, ctx):
        """Delete messages"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.clean)

    @clean.command()
    async def last(self, ctx, number: int = 1):
        """Delete the latest messages on the current channel"""
        deleted = await ctx.channel.purge(limit=number)
        await ctx.send(md.code_block(_('Deleted {nb} messages').format(nb=len(deleted))), delete_after=MEDIUM_DELAY)

    @clean.command()
    async def old(self, ctx, weeks: int = 16):
        """Delete old messages on the current channel"""
        deleted = await ctx.channel.purge(before=(datetime.now() - timedelta(weeks=weeks)))
        await ctx.send(md.code_block(_('Deleted {nb} messages').format(nb=len(deleted))), delete_after=MEDIUM_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)


def setup(client):
    client.add_cog(Admin(client))
