from datetime import datetime

import discord
from discord.ext import commands

from cogs import _, COLOR, LONG_DELAY, SHORT_DELAY, MAINTAINER_URL, WEBSITE_URL
from cogs import Markdown as md


class Bot(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.group()
    async def bot(self, ctx):
        """Manage bot"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.bot)

    @bot.command()
    @commands.has_permissions(administrator=True)
    async def ping(self, ctx):
        """Get bot latency"""
        msg = _('latency: {latency}ms').format(latency=round(ctx.bot.latency * 1000))
        await ctx.send(md.code_block(msg), delete_after=LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @bot.command()
    @commands.has_permissions(administrator=True)
    async def uptime(self, ctx):
        """Get bot uptime"""
        msg = str(datetime.now() - self.client.start).split(".")[0]
        await ctx.send(md.code_block(msg), delete_after=LONG_DELAY)
        await ctx.message.delete(delay=SHORT_DELAY)

    @bot.command(aliases=['info'])
    async def id(self, ctx):
        """Display bot id card"""
        appInfo = await ctx.bot.application_info()
        bot = ctx.bot.user
        links = md.ahref(MAINTAINER_URL, _('Submit Bug Report or Enhancement')) + '\n'
        links += md.ahref(WEBSITE_URL, _('Source code'))

        botId = discord.Embed(title=_("Bot's identity card"),
                              color=COLOR)

        botId.set_author(name=bot.display_name, url=WEBSITE_URL, icon_url=bot.avatar_url)
        botId.set_thumbnail(url=bot.avatar_url)

        botId.add_field(name=_('Bot name'), value=bot.display_name)
        botId.add_field(name=_('Discriminator'), value=bot.discriminator)
        botId.add_field(name=_('Id'), value=bot.id)
        botId.add_field(name=_('Create at'), value=bot.created_at.date())
        botId.add_field(name=_('Owner'), value=appInfo.owner.mention)
        botId.add_field(name=_('Application'), value=appInfo.name + ' - ' + appInfo.description)
        botId.add_field(name=_('Links') + ' - ' + _('Help us to improve your bot!'), value=links)

        await ctx.send(embed=botId)

    @bot.command(aliases=['enhancement'])
    async def bug(self, ctx):
        """Submit bug report or enhancement"""
        bot = ctx.bot.user

        embed = discord.Embed(title=_('Help us to improve your bot!'),
                              color=COLOR)

        embed.set_author(name=bot.display_name, url=WEBSITE_URL, icon_url=bot.avatar_url)
        embed.set_thumbnail(url=bot.avatar_url)

        embed.add_field(name=_('Submit Bug Report or Enhancement'),
                        value=md.ahref(MAINTAINER_URL, _('Submit Bug Report or Enhancement')),
                        inline=False)

        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(Bot(client))
