import discord
from discord.ext import commands

from cogs import _, COLOR
from cogs import Markdown as md

DISCORD_ICON_URL = 'https://www.shareicon.net/data/128x128/2017/06/21/887435_logo_512x512.png'


class Discord(commands.Cog):

    def __init__(self, client):
        self.client = client
        # Discord slash commands cheat sheet
        slash_cmd = {'/nick <nickname>': _('Change nickname on this server.'),
                     '/giphy ': _('Search animated GIFs on the web.'),
                     '/tenor ': _('Search animated GIFs on the web.'),
                     '/me <text>': _('Displays text with emphasis.'),
                     '/tableflip [message]': _('Appends (╯°□°）╯︵ ┻━┻ to your message.'),
                     '/unflip [message]': _('Appends ┬─┬ ノ( ゜-゜ノ) to your message.'),
                     '/shrug [message]': _('Appends ¯\x5C_(ツ)_/¯ to your message.'),
                     '/spoiler <message>': _('Marks your message as a spoiler.'),
                     '/tts <message>':
                         _('Use text-to-speech to read the message to all members currently viewing the channel.')}
        slash_cmd = '\n'.join('`{0:<20}`    {1}'.format(k, v) for k, v in slash_cmd.items())
        # Discord notification commands cheat sheet
        mention = {'@<member>': _('Mention a member.'),
                   '@everyone': _('Notify everyone who has permission to view this channel.'),
                   '@here': _('Notify everyone online who has permission to view this channel.')}
        mention = '\n'.join('`{0:<20}`    {1}'.format(k, v) for k, v in mention.items())

        # Markdown syntax
        self.mdsyntax = discord.Embed(title=_('Discord Markdown cheat sheet'),
                                    description=_('A digest of Discord Markdown syntax.'),
                                    color=COLOR)
        self.mdsyntax.add_field(name=_('italic'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.italic('an example')))
        self.mdsyntax.add_field(name=_('bold'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.bold('an example')))
        self.mdsyntax.add_field(name=_('underline'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.underline('an example')))
        self.mdsyntax.add_field(name=_('strikethrough'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.strikethrough('an example')))
        self.mdsyntax.add_field(name=_('inline code block'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.code('an example')))
        self.mdsyntax.add_field(name=_('spoiler'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.spoiler('an example')))
        self.mdsyntax.add_field(name=_('quote'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.quote('an example')))
        self.mdsyntax.add_field(name=_('inline url'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.href('https://discordapp.com')))
        self.mdsyntax.add_field(name=_('mix'),
                              value='{0}\n```\n{0:<20}\n```'.format(md.underline(md.bold(md.italic('an example')))))
        self.mdsyntax.set_footer(text='Discord', icon_url=DISCORD_ICON_URL)

        # Slash commands
        self.commands = discord.Embed(title=_('Discord slash commands cheat sheet'),
                                      description=_('A digest of Discord slash commands.'),
                                      color=COLOR)
        self.commands.add_field(name=_('Slash commands'), value=slash_cmd)
        self.commands.set_footer(text='Discord', icon_url=DISCORD_ICON_URL)

        # Mentions and notifications
        self.notifications = discord.Embed(title=_('Discord notification commands cheat sheet'),
                                           description=_('A digest of Discord notification commands.'),
                                           color=COLOR)
        self.notifications.add_field(name=_('Notification commands'), value=mention)
        self.notifications.set_footer(text='Discord', icon_url=DISCORD_ICON_URL)

    @commands.group()
    async def discord(self, ctx):
        """Discord cheat sheet"""
        if ctx.invoked_subcommand is None:
            await ctx.invoke(self.syntax)

    @discord.command(aliases=['markdown', 'md'])
    async def syntax(self, ctx):
        """Discord Markdown cheat sheet"""
        await ctx.send(embed=self.mdsyntax)

    @discord.command(aliases=['commands', 'cmd'])
    async def command(self, ctx):
        """Discord slash commands cheat sheet"""
        await ctx.send(embed=self.commands)

    @discord.command(aliases=['mentions', 'notification', 'notifications', 'notif'])
    async def mention(self, ctx):
        """Discord notification commands cheat sheet"""
        await ctx.send(embed=self.notifications)


def setup(client):
    client.add_cog(Discord(client))
