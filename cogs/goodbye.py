from discord.ext import commands

from cogs import Console as log


class Goodbye(commands.Cog):

    # Events -->
    @commands.Cog.listener()
    async def on_member_remove(self, member):
        log.info('Member {0.id} leaves {0.guild.id}'.format(member))
    # Events <--


def setup(client):
    client.add_cog(Goodbye(client))
