from collections import OrderedDict

import discord
from discord.ext import commands

from cogs import _, COLOR
from cogs import Markdown as md


BOOLEAN_EMOJIS = OrderedDict([(':heavy_check_mark:', '\U00002714'),
                              (':heavy_multiplication_x:', '\U00002716')])

DECIMAL_EMOJIS = OrderedDict([(':0:', '\U00000030\U000020E3'), (':1:', '\U00000031\U000020E3'),
                              (':2:', '\U00000032\U000020E3'), (':3:', '\U00000033\U000020E3'),
                              (':4:', '\U00000034\U000020E3'), (':5:', '\U00000035\U000020E3'),
                              (':6:', '\U00000036\U000020E3'), (':7:', '\U00000037\U000020E3'),
                              (':8:', '\U00000038\U000020E3'), (':9:', '\U00000039\U000020E3')])


class Poll(commands.Cog):

    @commands.command(aliases=['survey', 'vote'])
    async def poll(self, ctx, question: str, *answers):
        """Take a poll"""
        embed = discord.Embed(color=COLOR)
        embed.set_author(name=ctx.author.display_name,
                         icon_url=ctx.author.avatar_url)

        if answers:
            # poll with decimal emojis
            answer_count = len(answers)
            if answer_count == 1 or answer_count > 10:
                await ctx.send(md.code_block('You need 0, 2 or more answers.'))
                return 0
            # format poll
            answers = list(answers)
            for i in range(answer_count):
                answers[i] = DECIMAL_EMOJIS[f':{i}:'] + ' ' + answers[i]
            embed.add_field(name=md.bold(question), value='\n'.join(answers))
            # send poll
            msg = await ctx.send(embed=embed)
            # add reactions
            for i in range(answer_count):
                await msg.add_reaction(DECIMAL_EMOJIS[f':{i}:'])
        else:
            # poll with boolean emojis
            # format poll
            embed.add_field(name=md.bold(question), value=_('what do you think?'))
            # send poll
            msg = await ctx.send(embed=embed)
            # add reactions
            await msg.add_reaction(BOOLEAN_EMOJIS[':heavy_check_mark:'])
            await msg.add_reaction(BOOLEAN_EMOJIS[':heavy_multiplication_x:'])


def setup(client):
    client.add_cog(Poll(client))
