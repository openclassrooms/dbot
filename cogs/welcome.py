import sqlite3

import discord
from discord.ext import commands

from cogs import _, DB_NAME, COLOR
from cogs import Console as log
from cogs import Markdown as md


class Welcome(commands.Cog):

    def __init__(self, client):
        self.client = client

        self.vars = {'member': '', 'user': '', 'mention': '',
                     'guild': '', 'server': '', 'channel': '',
                     'welcome_channel': '', 'system_channel': ''}

        self.default = {'msg': _('Welcome {member} to {guild}!'),
                        'ext_msg': _('Present yourself on {channel}.')}

        self.welcome_msg = {4: _('Hi {mention}!'),
                            8: _('Hello {mention}!'),
                            16: _('Welcome {mention}!'),
                            32: _('Welcome among us {mention}!'),
                            64: _('Hello and welcome {mention}!'),
                            128: _('Hello and welcome among us {mention}! You are our 128th member!'),
                            256: _('Hello and welcome among us {mention}! You are our 256th member worth celebrating!'),
                            512: _('Our 512th member! We applaud {mention} for welcoming him! :clap:'),
                            1024: _("We applaud {mention}! :clap: It's our 1024th member! Welcome among us!"),
                            2048: _(''),
                            4096: _(''),
                            8192: _(''),
                            16384: _(''),
                            32768: _(''),
                            65536: _('')}

        self.conn = sqlite3.connect(DB_NAME)

        with self.conn:
            self.conn.execute('''CREATE TABLE IF NOT EXISTS welcome(
            guild_id UNSIGNED BIG INT PRIMARY KEY,
            channel TEXT,
            message TEXT)''')

    # Welcome commands -->
    @commands.group()
    @commands.has_permissions(administrator=True)
    async def welcome(self, ctx):
        """Configure the welcome event when a member join the guild"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.welcome)

    @welcome.command()
    async def purge(self, ctx):
        """Delete custom welcome settings"""
        with self.conn:
            self.conn.execute('''UPDATE welcome SET message='', channel='' WHERE guild_id=?''',
                              (ctx.message.guild.id,))

    # Setters
    @welcome.group()
    async def set(self, ctx):
        """Set custom welcome event settings"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.set)

    @set.command(aliases=['msg'])
    async def message(self, ctx, *, msg=None):
        """Set a custom private welcome message"""
        if msg is None:
            await ctx.send_help(self.message)
        msg = msg.strip()
        if msg == "''" or msg == '""':
            await ctx.send_help(self.message)
        else:
            with self.conn:
                self.conn.execute('''INSERT INTO welcome (guild_id, message)
                VALUES (?, ?) ON CONFLICT(guild_id) DO UPDATE SET message=excluded.message;''',
                                  (ctx.message.guild.id, md.reverse_escape(md.dequote(msg))))

    @set.command()
    async def channel(self, ctx, channel: discord.TextChannel):
        """Set the welcome channel"""
        with self.conn:
            self.conn.execute('''INSERT INTO welcome (guild_id, channel)
                VALUES (?, ?) ON CONFLICT(guild_id) DO UPDATE SET channel=excluded.channel;''',
                              (ctx.message.guild.id, channel.mention))

    # Getters
    @welcome.group()
    async def get(self, ctx):
        """Display settings"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.get)

    @get.command(aliases=['configuration', 'cfg'])
    async def config(self, ctx):
        """Display current welcome event settings"""
        cursor = self.conn.cursor()
        cursor.execute('''SELECT message, channel FROM welcome WHERE guild_id=?''',
                       (ctx.message.guild.id,))
        data = cursor.fetchone()
        msg = '-'
        welcome_channel = '-'
        if data:
            msg, welcome_channel = data
            if msg and msg.strip():
                msg = msg.strip()
            else:
                msg = '-'
            if welcome_channel and welcome_channel.strip():
                welcome_channel = welcome_channel.strip()
            else:
                welcome_channel = '-'

        default_msg = self.default['msg'] + ' ' + self.default['ext_msg']
        if ctx.author.guild.system_channel is not None:
            system_channel = ctx.author.guild.system_channel.mention
        else:
            system_channel = '-'
        more_help = _("Get all available variables with command `{command}`.").format(command='!welcome get vars')

        embed = discord.Embed(title=_('Welcome event settings'), color=COLOR)
        embed.add_field(name=_('Custom welcome message'), value=msg, inline=False)
        embed.add_field(name=_('Default welcome message'), value=default_msg, inline=False)
        embed.add_field(name=_('Custom welcome channel'), value=welcome_channel, inline=False)
        embed.add_field(name=_('Default welcome channel (system channel)'), value=system_channel, inline=False)
        embed.add_field(name=_('Get more help'), value=more_help, inline=False)

        await ctx.send(embed=embed)

    @get.command(aliases=['variables'])
    async def vars(self, ctx):
        """Display available variables"""
        member = _('`member`: display user name (ex: {example})').format(example=ctx.author.display_name)
        member += '\n' + _('`user`: alias of `member`')
        member += '\n' + _('`mention`: mention user name (ex: {example})').format(example=ctx.author.mention)
        channel = _('`welcome_channel`: welcome channel mention if it is set (ex: {example})').format(
            example=ctx.message.channel.mention)
        channel += '\n' + _('`system_channel`: system channel mention if it set (ex: {example})').format(
            example=ctx.message.channel.mention)
        channel += '\n' + _('`channel`: welcome channel if it is set else system channel (ex: {example})').format(
            example=ctx.message.channel.mention)

        guild = '\n' + _('`guild`: guild name (ex: {example})').format(example=ctx.guild.name)
        guild += '\n' + _('`server`: alias of `guild`')

        embed = discord.Embed(title=_('Available variables'), color=COLOR)
        embed.add_field(name=_('Member'), value=member, inline=False)
        embed.add_field(name=_('Channel'), value=channel, inline=False)
        embed.add_field(name=_('Guild'), value=guild, inline=False)

        await ctx.send(embed=embed)
    # Welcome commands <--

    # Welcome event -->
    @commands.Cog.listener()
    async def on_member_join(self, member):
        self.vars['member'] = member.display_name
        self.vars['user'] = self.vars['member']
        self.vars['mention'] = member.mention
        self.vars['guild'] = member.guild.name
        self.vars['server'] = self.vars['guild']

        cursor = self.conn.cursor()
        cursor.execute('''SELECT message FROM welcome WHERE guild_id=?''',
                       (member.guild.id,))

        msg = cursor.fetchone()
        if not msg or not msg[0]:
            msg = self.default['msg']
            if member.guild.system_channel is not None:
                msg += ' ' + self.default['ext_msg']
        else:
            msg = msg[0]

        if member.guild.system_channel is not None:
            self.vars['system_channel'] = '{0}'.format(member.guild.system_channel.mention)
            self.vars['channel'] = self.vars['system_channel']
            self.vars['welcome_channel'] = self.vars['system_channel']

        msg = msg.format(**self.vars)

        await member.create_dm()
        await member.dm_channel.send(msg)

        try:
            welcome_msg = self.welcome_msg[len(member.guild.members)]
            welcome_channel = member.guild.get_channel(int(self.vars['channel'][2:-1]))

            await welcome_channel.send(welcome_msg.format(**self.vars))
        except KeyError as e:
            pass

        log.info('New member {0.id} joins {0.guild.id}'.format(member))
    # Welcome event <--


def setup(client):
    client.add_cog(Welcome(client))
