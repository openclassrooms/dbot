#!/usr/bin/python3


import traceback
from os import getenv
from datetime import datetime, timedelta

from discord.ext.commands import Bot, CommandNotFound, CommandInvokeError, \
    CheckFailure, ExtensionNotLoaded, ExtensionAlreadyLoaded, ExtensionError, \
    check, MissingRequiredArgument, BadArgument

from cogs import _, cogs_available, cogs_enabled, MAINTAINER_URL
from cogs import Console as log
from cogs import Markdown as md


# Configure log level
log.set_log_level(getenv('BOT_LOG_LEVEL', 'INFO'))

# Instantiate bot
bot = Bot(command_prefix=getenv('BOT_COMMAND_PREFIX', '!'))


# Core events -->
@bot.event
async def on_ready():
    log.info('{0} has connected to Discord'.format(bot.user.name))
    bot.start = datetime.now()


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, BadArgument):
        await ctx.send_help(ctx.command)
    elif isinstance(error, CommandNotFound):
        log.debug('{0}'.format(error))
    elif isinstance(error, CheckFailure):
        log.debug('{0}'.format(error))
        await ctx.send(md.code_block("You don't have the permission - {0}".format(error)))
    elif isinstance(error, CommandInvokeError) and \
            '404 NOT FOUND (error code: 10008)' in '{0}'.format(error):
        log.warning('{0}'.format(error))
    elif isinstance(error, CommandInvokeError):
        log.warning('{0}'.format(error))
        await ctx.send(md.code_block('{0}'.format(error)))
    elif isinstance(error, MissingRequiredArgument):
        log.warning('{0}'.format(error))
        await ctx.send(md.code_block('{0}'.format(error)))
    else:
        log.critical('\n'.join(traceback.format_exception(Exception, error, error.__traceback__)))
# Core events <--


# Core commands -->
async def is_owner(ctx):
    appInfo = await bot.application_info()
    return ctx.author.id == appInfo.owner.id


@bot.group(aliases=['root'])
@check(is_owner)
async def core(ctx):
    """Manage core bot (owner only)"""
    if ctx.invoked_subcommand is None:
        await ctx.send_help(core)


@core.command()
async def ping(ctx):
    """Get bot latency"""
    msg = 'latency: {latency}ms'.format(latency=round(ctx.bot.latency * 1000))
    await ctx.send(md.code_block(msg))


@core.command(aliases=['information'])
async def info(ctx):
    """Display bot information"""
    app_info = await bot.application_info()

    bot_id = "Bot:\n"
    bot_id += "  {0:<16}{1}\n".format('Name:', bot.user.display_name)
    bot_id += "  {0:<16}{1}\n".format('Discriminator:', bot.user.discriminator)
    bot_id += "  {0:<16}{1}\n".format('Id:', bot.user.id)
    bot_id += "  {0:<16}{1}\n".format('Create at:', bot.user.created_at.date())
    bot_id += "Application:\n"
    bot_id += "  {0:<16}{1}\n".format('Name:', app_info.name)
    bot_id += "  {0:<16}{1}\n".format('Description:', app_info.description)
    bot_id += "Owner:\n"
    bot_id += "  {0:<16}{1}\n".format('Name:', app_info.owner.name)
    bot_id += "  {0:<16}{1}\n".format('Id:', app_info.owner.id)
    bot_id += "Submit bug report and enhancement:\n"
    bot_id += "  {0}\n".format(MAINTAINER_URL)

    await ctx.send(md.code_block(bot_id))


@core.command()
async def status(ctx):
    """Get bot status"""
    app_info = await bot.application_info()

    bot_status = "Bot:\n"
    bot_status += "  {0:<16}{1}\n".format('Started since:', str(datetime.now()).split(".")[0])
    bot_status += "  {0:<16}{1}\n".format('Uptime:', str(datetime.now() - bot.start).split(".")[0])
    bot_status += "  {0:<16}{1}\n".format('Status:', ctx.guild.get_member(bot.user.id).status)
    bot_status += "  {0:<16}{1}\n".format('Is public:', app_info.bot_public)
    bot_status += "  {0:<16}{1}\n".format('Guilds:', len(bot.guilds))
    bot_status += "  {0:<16}{1}\n".format('Users:', len(bot.users))

    await ctx.send(md.code_block(bot_status))


@core.command()
async def available(ctx):
    """List available extensions"""
    msg = 'Available extensions:\n  '
    msg += '\n  '.join(cogs_available)
    await ctx.send(md.code_block(msg))


@core.command()
async def loaded(ctx):
    """List loaded extensions"""
    if cogs_enabled:
        msg = 'Loaded extensions:\n  '
        msg += '\n  '.join(cogs_enabled)
        await ctx.send(md.code_block(msg))
    else:
        await ctx.send(md.code_block('No loaded extensions.'))


@core.command()
async def unloaded(ctx):
    """List unloaded extensions"""
    cogs_unloaded = cogs_available.symmetric_difference(cogs_enabled)
    if cogs_unloaded:
        msg = 'Unloaded extensions:\n  '
        msg += '\n  '.join(cogs_unloaded)
        await ctx.send(md.code_block(msg))
    else:
        await ctx.send(md.code_block('All extensions are loaded.'))


@core.command()
async def load(ctx, extension):
    """Load an extension"""
    try:
        bot.load_extension(f'cogs.{extension}')
        cogs_enabled.add(extension)
        await ctx.send(md.code_block('{0} extension is now loaded.'.format(extension)))
    except ExtensionAlreadyLoaded as e:
        log.debug('{0}'.format(e))
        await ctx.send(md.code_block('{0}'.format(e)))


@core.command()
async def unload(ctx, extension):
    """Unload an extension"""
    try:
        bot.unload_extension(f'cogs.{extension}')
        cogs_enabled.remove(extension)
        await ctx.send(md.code_block('{0} extension was unloaded.'.format(extension)))
    except ExtensionNotLoaded as e:
        log.debug('{0}'.format(e))
        await ctx.send(md.code_block('{0}'.format(e)))
# Core commands <--


# Load extensions -->
for cog in cogs_available:
    try:
        bot.load_extension(f'cogs.{cog}')
        cogs_enabled.add(cog)
    except ExtensionError as e:
        log.error('Cannot be load extension {0} {1}'.format(cog, e))
# Load extensions <--


# Run bot
bot.run(getenv('DISCORD_TOKEN'))
